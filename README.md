# snakemake_demo

Demo for using snakemake

## Setup Pipeline

HOW TO setup the Pipeline

### Prerequisites
 
- anaconda installed
- clone repository
- add “static” folder in snakemake_demo/workflow


### Install snakemake

cd into repository and run the following command:

$ conda env create -f workflow/env/snakemake.yml

### activate environment

$ conda activate snakemake

### run pipeline

$ snakemake predict_all --use-conda

