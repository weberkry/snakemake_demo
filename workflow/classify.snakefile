
rule train_model:
	input:
		X_train = "Output/Train_Test_Set/X_train.npy",
		X_val = "Output/Train_Test_Set/X_val.npy",
		y_train = "Output/Train_Test_Set/y_train.csv",
		y_val = "Output/Train_Test_Set/y_val.csv"
	params:
		seed = config["seed"]
	output:
		model = directory("Output/model/CNN_{optimizer}_{epochs}_{batchsize}"),
		history = "Output/Training/CNN_history_{optimizer}_{epochs}_{batchsize}.csv",
		eval = "Output/Training/CNN_eval_{optimizer}_{epochs}_{batchsize}.pdf",
		loss = "Output/Training/CNN_loss_{optimizer}_{epochs}_{batchsize}.pdf"
	conda: 
		"env/classify.yml"
	script:
		"scripts/train_model.py"

rule predict_image:
	input:
		model = "Output/model/CNN_{optimizer}_{epochs}_{batchsize}",
		classes = "Output/Train_Test_Set/y_train.csv",
		image = "static/predict/{img}.jpg"
	output:
		result = "Output/Prediction/{img}/result_CNN_{optimizer}_{epochs}_{batchsize}_{img}.txt",
		plot = "Output/Prediction/{img}/result_CNN_{optimizer}_{epochs}_{batchsize}_{img}.pdf"
	conda: 
		"env/classify.yml"
	script:
		"scripts/predict_image.py"

rule predict_all:
	input:
		expand("Output/Prediction/{img}/result_CNN_adam_10_128_{img}.pdf", img=config["image"]),
		expand("Output/Prediction/{img}/result_CNN_adam_10_256_{img}.pdf", img=config["image"]),
		expand("Output/Prediction/{img}/result_CNN_sgd_15_256_{img}.pdf", img=config["image"])

#rule PDFreport:
 #   input:
 #		plot = "Output/Prediction/{img}/result_CNN_{optimizer}_{epochs}_{batchsize}_{img}.pdf",
#		classes = "Output/Preprocessing/class.csv",
#		prediction = "Output/Prediction/result_CNN_{optimizer}_{epochs}_{batchsize}_{img}.txt",
#		Train_test_distribution = "Output/Train_Test_Set/distribution_train_set.pdf",
#		history = "Output/Training/CNN_history_{optimizer}_{epochs}_{batchsize}.csv",
#		eval = "Output/Training/CNN_eval_{optimizer}_{epochs}_{batchsize}.pdf",
#		loss = "Output/Training/CNN_loss_{optimizer}_{epochs}_{batchsize}.pdf"
 #   output:
  #      "results/reports/Report_CNN_{optimizer}_{epochs}_{batchsize}_{img}.pdf"
   # resources: pdfReport=1
   # conda: "envs/PDFreport.yaml"
   # script:
   #     "scripts/makePDFreport.R"

rule random_script:
	params:
		"Bingo"
	conda: 
		"env/classify.yml"
	shell:
		". scripts/random_script.sh {params}"
